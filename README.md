FreeIPA Server
==============
Installs and configures FreeIPA server

Supported Operating Systems
---------------------------
* CentOS 7

Requirements
------------
No Requirements

Role Variables
--------------
```
---
firewall_freeipa_server_services:
  - name: http
    description: 'HTTP is the protocol used to serve Web pages'
    priority: 1
    zones: '{{ freeipa_server_firewall_zones }}'
    rules:
      - protocol: tcp
        port: 80
  - name: https
    description: 'HTTPS is the protocol used to serve Web pages over SSL/TLS'
    priority: 1
    zones: '{{ freeipa_server_firewall_zones }}'
    rules:
      - protocol: tcp
        port: 443
  - name: ldap
    description: 'Lightweight Directory Access Protocol (LDAP) server'
    priority: 1
    zones: '{{ freeipa_server_firewall_zones }}'
    rules:
      - protocol: tcp
        port: 389
  - name: ldaps
    description: 'Lightweight Directory Access Protocol (LDAP) server over SSL'
    priority: 1
    zones: '{{ freeipa_server_firewall_zones }}'
    rules:
      - protocol: tcp
        port: 636
  - name: kerberos
    description: 'Kerberos network authentication protocol server'
    priority: 1
    zones: '{{ freeipa_server_firewall_zones }}'
    rules:
      - protocol: tcp
        port: 88
      - protocol: udp
        port: 88
  - name: kpasswd
    description: 'Kerberos password (Kpasswd) server'
    priority: 1
    zones: '{{ freeipa_server_firewall_zones }}'
    rules:
      - protocol: tcp
        port: 464
      - protocol: udp
        port: 464
  - name: kadmin
    description: 'Kerberos Administration Protocol'
    priority: 1
    zones: '{{ freeipa_server_firewall_zones }}'
    rules:
      - protocol: tcp
        port: 749
  - name: dns
    description: 'Domain Name System'
    priority: 1
    zones: '{{ freeipa_server_firewall_zones }}'
    rules:
      - protocol: tcp
        port: 53
      - protocol: udp
        port: 53
  - name: ntp
    description: 'Network Time Protocol'
    priority: 1
    zones: '{{ freeipa_server_firewall_zones }}'
    rules:
      - protocol: udp
        port: 123
```

Role Defaults
-------------
```
---
freeipa_server_firewall_zones:
  - '{{ firewall_default_zone }}'
```

Dependencies
------------
* firewall

Configuring FreeIPA
-------------------
To configure the FreeIPA realm, the following variables need to be set:

	realm_name: <string> the name of the realm
	domain_name: <string> the domain name for the realm
	dm_password: <string> the domain admin password
	admin_password: <string> the LDAP admin password

Testing
-------
Tests can be found in the `tests` directory. Current tests validate the following:

* yamllint
* Syntax check
* Provisioning (Fails due to IPv6 conflicts with Docker)

### Local Testing
Local testing can be done by running the `docker-compose.yml` file found in the `tests` directory

### CI Testing
CI testing configuration can be found in `.gitlab-ci.yml` in the root directory

License
-------
ISC

Author Information
------------------
CV Admins <cvadmins@utdallas.edu>
